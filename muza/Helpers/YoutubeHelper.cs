﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using muza.Models;

namespace muza.Helpers
{
    public static class YoutubeHelper
    {
        public static string GetYoutubeEmbededLink(string url, int playerid = -1)
        {
            var input = url;


            const string pattern = @"(?:https?:\/\/)?(?:www\.)?(?:(?:(?:youtube.com\/watch\?[^?]*v=|youtu.be\/)([\w\-]+))(?:[^\s?]+)?)";
            string replacement = string.Format("<iframe title='YouTube video player' {0} width='480' height='390' src='http://www.youtube.com/embed/$1?enablejsapi=1' frameborder='0' allowfullscreen='1'></iframe>", playerid >= 0 ? "id='player" + playerid +"'" : "");

            var rgx = new Regex(pattern);
            var result = rgx.Replace(input, replacement);

            return result;
        }

        public static YoutubeModel GetEmbeddedYoutubeResult(string url, int playerid = -1)
        {
            var result = new YoutubeModel { title = GetYoutubeTitle(url), result = GetYoutubeEmbededLink(url, playerid), url = url };
            return result;
            
        }

        public static IEnumerable<YoutubeModel> GetEmbeddedYoutubeResults(IEnumerable<string> urls)
        {
            var result = new List<YoutubeModel>();

            foreach (var url in urls)
            {
                var model = GetEmbeddedYoutubeResult(url, urls.IndexOf(url,StringComparer.InvariantCultureIgnoreCase));
                result.Add(model);
            }

            return result;
        }

        public static string GetYoutubeTitle(string url)
        {
            string id = GetArgs(url, "v", '?');
            WebClient client = new WebClient();
            return GetArgs(client.DownloadString("http://youtube.com/get_video_info?video_id=" + id), "title", '&');
        }

        private static string GetArgs(string args, string key, char query)
        {
            int iqs = args.IndexOf(query);
            string querystring = null;

            if (iqs != -1)
            {
                querystring = (iqs < args.Length - 1) ? args.Substring(iqs + 1) : String.Empty;
                NameValueCollection nvcArgs = HttpUtility.ParseQueryString(querystring);
                return nvcArgs[key];
            }
            return String.Empty; // or throw an error
        }
    }
}