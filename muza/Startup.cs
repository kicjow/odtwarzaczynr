﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(muza.Startup))]
namespace muza
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
