﻿using System.Web;
using System.Web.Optimization;

namespace muza
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/myScripts").Include(
                   "~/Scripts/myScripts/DownloadRawAudio.js",
                "~/Scripts/myScripts/YoutubeHelper.js",
                 "~/Scripts/myScripts/ButtonManager.js",
                 "~/Scripts/myScripts/MenuManager.js",
                  "~/Scripts/myScripts/SavePlaylist.js",
                "~/Scripts/myScripts/AppManager.js"));

        }
    }
}
