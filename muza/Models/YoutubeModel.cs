﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace muza.Models
{
    public class YoutubeModel
    {
        public string title { get; set; }
        public string url { get; set; }
        public string result { get; set; }
    }
}