﻿// <reference path="../typings/jquery/jquery.d.ts" />

module ButtonManager {
    export class ButtonManager {
        private inputCount: number;

        constructor() {
            this.inputCount = 0;
        }

        public addNewInput = (divContainer: JQuery, e: JQueryEventObject): void  => {
            this.inputCount++;
            var newInput: HTMLInputElement = document.createElement("input");
            newInput.setAttribute("type", "text");
            newInput.setAttribute("name", "url" + this.inputCount);
            newInput.setAttribute("class", "form-control");

            divContainer.append(newInput);

        }

    }

}