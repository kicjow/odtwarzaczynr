﻿// <reference path="../typings/jquery/jquery.d.ts" />
// <reference path="../typings/youtube/youtube.d.ts" />

import YtHelper = YoutubeHelper.YoutubeHelper;
import YoutubeApiPlayer = YtMenuManager.YoutubeApiPlayer;
import MenuManager = YtMenuManager.MenuManager;
import ButtonMng = ButtonManager.ButtonManager;
import DownloadMng = RawAudioManager.RawAudioDownloader;
import SaveButton = SavePlaylistManager.PlaylistSaver;

module AppManager {
    export class App {

        private formJq: JQuery;
        private playersMaxIndex: number;
        public menuManager: MenuManager;

        constructor() {
            this.formJq = $("form#audioVideoForm");
            this.playersMaxIndex = 0;
            $("#Embedded").on("click", this.embeddedOnClickHandler);
            this.menuManager = new MenuManager();

        }

        private embeddedOnClickHandler = (e: JQueryEventObject): void => {
            e.preventDefault();
            this.createPlayers(this.formJq);
        }

        private createPlayers = (formSource: JQuery): void => {
            var formDataArray: JQuerySerializeArrayElement[] = formSource.serializeArray();
            var dataValuesArray: string[] = this.getValues(formDataArray);

            var divsIds: string[] = this.CreateDivs(dataValuesArray.length);
            var ytPlayers: YoutubeApiPlayer[] = this.CreateYtPlayers(dataValuesArray);

            this.playersMaxIndex += dataValuesArray.length;
            this.menuManager.Init(ytPlayers, divsIds);


        }

        private CreateYtPlayers(dataArray: string[]): YoutubeApiPlayer[] {
            var players: YoutubeApiPlayer[] = [];

            for (var i = 0; i < dataArray.length; i++)
            {
                var url: string = dataArray[i];
                var pattern: string = 'player' + (i + this.playersMaxIndex);
                var youtubeId = YtHelper.parseYoutubeUrl(url);

                var events: YT.Events = {
                    onStateChange: this.menuManager.onPlayerStateChange
                }

                var options: YT.PlayerOptions = {
                    height: '390',
                    width: '640',
                    videoId: youtubeId,
                    events: events
                }

                var player: YT.Player = new YT.Player(pattern, options);
                players.push(player);
            }

            return players;
        }

        private CreateDivs(length: number): string[] {
            var container: JQuery = $('#youtubeContainer');
            var htmlToAppend = "";
            var items: string[] = [];

            for (var i = 0; i < length; i++)
            {
                var pattern = 'player' + (i + this.playersMaxIndex);
                var singlePlayerContainer = pattern;
                items[i] = singlePlayerContainer;
            }

            htmlToAppend = "<div id='" + items.join("'></div><div id='") + "'></div>";
            container.append(htmlToAppend);

            return items;
        }

        private getValues(source: JQuerySerializeArrayElement[]): string[] {
            var result: string[] = [];

            for (var n: number = 0; n < source.length; n++)
            {
                if (source[n].value && source[n].value != "")
                {
                    result.push(source[n].value);
                }
            }

            return result;

        }

    }
}

import AppLogic = AppManager.App;

$(document).ready(function () {
    var app: AppLogic = new AppLogic();

    var btnMng = new ButtonMng();

    var button = $('#addNewInputButton');
    var divContainer = $('#buttonDivContainer');

    button.click((e) => btnMng.addNewInput(divContainer, e));

    var rawAudioMng = new DownloadMng();

    var button = $("#sendPlaylistButton");
    button.unbind('click').bind('click', () => {
        var x = new SaveButton(app.menuManager);
    });

});