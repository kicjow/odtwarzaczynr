﻿module YtMenuManager {

    export interface YoutubeApiPlayer {
        playVideo(): void;
        stopVideo(): void;
        clearVideo(): void;
    }

    class PlayerContainer {

        constructor(public youtubePlayer: YoutubeApiPlayer, public correspondingDivId: string) { }

    }

    export class MenuManager {

        private playersContainers: PlayerContainer[];
        private currentPlayerContainer: PlayerContainer;

        constructor() {
            this.playersContainers = [];
            this.currentPlayerContainer = null;

            this.bindButtons()

        }

        public Init(youtubePlayers: YoutubeApiPlayer[], correspondingDivsId: string[]): void {

            var playersCont: PlayerContainer[] = this.createContainers(youtubePlayers, correspondingDivsId);
            this.playersContainers = this.playersContainers.concat(playersCont);


        }

        private bindButtons = (): void => {
            $("#playButton").unbind('click').bind('click', (e) => {
                if (this.playersContainers.length === 0) {
                    return;
                } else if (this.playersContainers.length > 0 && this.currentPlayerContainer == null) {
                    this.currentPlayerContainer = this.playersContainers[0];
                }

                this.play(this.currentPlayerContainer.correspondingDivId);

            });

            $("#nextButton").unbind("click").bind('click', (e) => {
                this.currentPlayerContainer.youtubePlayer.stopVideo();
                this.playNext();
            });
        }

        private createContainers(players: YoutubeApiPlayer[], divsId: string[]): PlayerContainer[] {

            if (players.length != divsId.length) {
                alert("COŚ SIĘ WYJEBAŁO NA DŁUGOSCI TABLIC: MENU MANAGER - CREATE CONTAINERS");
            }
            var playersCont: PlayerContainer[] = [];

            for (var i = 0; i < players.length; i++) {

                var container: PlayerContainer = new PlayerContainer(players[i], divsId[i]);
                playersCont.push(container);
            }

            return playersCont;
        }

        public GetPlayer(divId: string): PlayerContainer {

            for (var i = 0; i < this.playersContainers.length; i++) {
                if (this.playersContainers[i].correspondingDivId === divId) {
                    return this.playersContainers[i];
                }
            }

            var id: number = this.getNumberId(divId);
            if (id + 1 >= this.playersContainers.length) {
                id = 0;
            }
            else {
                id++;
            }

            var newId = 'player' + id;

            return this.GetPlayer(newId);
        }

        public play = (id: string): void => {
            this.GetPlayer(id).youtubePlayer.playVideo();
        }

        public onPlayerStateChange = (event: YT.EventArgs): void  => {
            if (event.data === 0) {
                event.target.clearVideo();
                this.playNext();
            }

            if (event.data === 1) {
                var id = (<any>(event.target)).m.id;
                this.currentPlayerContainer = this.GetPlayer(id);
            }
        }

        private playNext = (): void => {
            var currentplayerid: string = this.currentPlayerContainer.correspondingDivId;
            var id: number = this.getNumberId(currentplayerid);

            if (id + 1 >= this.playersContainers.length) {
                id = 0;
            }
            else {
                id++;
            }

            var newId = 'player' + id;
            this.play(newId);
        }

        private getNumberId = (idString: string): number => {
            var id = parseInt(idString.substring(6));
            return id;
        }

        public GetAllPlayers = (): YoutubeApiPlayer[]  => {

            var result = [];

            for (var i = 0; i < this.playersContainers.length; i++)
            {
                result.push(this.playersContainers[i].youtubePlayer);
            }

            return result;
        }



    }
}