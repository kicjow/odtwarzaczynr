﻿// <reference path="../typings/jquery/jquery.d.ts" />

module SavePlaylistManager {

    export class PlaylistSaver {

        constructor(private menuMng: MenuManager) {

            this.SendUrls();

        }

        private GetPlaylistUrls = (): string[] => {

            var players: YoutubeApiPlayer[] = this.menuMng.GetAllPlayers();
            var result: string[] = [];

            for (var i = 0; i < players.length; i++) {
                var dirtySrc = <string>(<any>players[i]).c.src;
                var embeddedSrc = dirtySrc.split('?')[0];
                var slashSrc = embeddedSrc.split('//');
                var length = slashSrc.length;
                var id = slashSrc[length - 1];
                var url = "https://www.youtube.com/watch?v=" + id;

                result.push(url);
            }

            return result;
        }

        private SendUrls = (): void => {

            var urls = this.GetPlaylistUrls();
            var stringedUrls = JSON.stringify(urls);
            var name = (<HTMLInputElement>($('#playListNameInput')[0])).value;

            var newForm = <HTMLFormElement>document.createElement("form");
            var nameNode = <HTMLInputElement>document.createElement("input");

            newForm.action = "../api/YoutubeApi/SavePlaylist";
            newForm.method = "POST";

            nameNode.name = "name";
            nameNode.value = name;

            var urlsNode = <HTMLInputElement>document.createElement("input");

            urlsNode.name = "jsonUrls";
            urlsNode.value = stringedUrls;

            newForm.appendChild(nameNode);
            newForm.appendChild(urlsNode);
            newForm.submit();            

        }

    }

}