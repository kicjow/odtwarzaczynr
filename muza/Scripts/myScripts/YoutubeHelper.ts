﻿module YtPlayerManager {
    export class PlayerManager {

    }

}

module YoutubeHelper {
    export class YoutubeHelper {
        public static parseYoutubeUrl(url: string): string {
            var videoId = url.split('v=')[1];
            if (typeof videoId === 'undefined')
            {
                return "";
            }
            var ampersandPosition = videoId.indexOf('&');
            if (ampersandPosition != -1)
            {
                videoId = videoId.substring(0, ampersandPosition);
            }
            return videoId;
        }
    }
}