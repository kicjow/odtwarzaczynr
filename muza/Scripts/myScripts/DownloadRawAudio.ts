﻿// <reference path="../typings/jquery/jquery.d.ts" />

module RawAudioManager {
    export class RawAudioDownloader {

        private _url: string = "GetRawAudios";
        private _formId: string = "form#audioVideoForm";
        private _buttonId = "#audio";

        constructor() {
            $(this._buttonId).unbind('click').bind('click', (e) => this.sendForm(e));
        }

        public sendForm(e: JQueryEventObject) {

            var form: JQuery = $(this._formId);
            form.attr("action", this._url);
            form.unbind('submit').bind('submit', { form: form }, this.sendFormHandler);
            form.submit();
        }

        private sendFormHandler = (e: JQueryEventObject): void  => {
            e.preventDefault();
            var form = <JQuery>(e.data.form);
            var formDataArray = form.serializeArray();
            var dataValuesArray: string[] = [];

            formDataArray.forEach((e, index, array) => {
                dataValuesArray.push(e.value);
            });

            var formDataString: string = JSON.stringify( dataValuesArray);

            var newForm = <HTMLFormElement>document.createElement("form");
            var node = <HTMLInputElement>document.createElement("input");

            newForm.action = this._url;
            newForm.method = "POST";
           
            node.name = "urls";
            node.value = formDataString;

            newForm.appendChild(node);
            newForm.submit();
        }


    }

}