﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Results;
using System.Web.Mvc;
using muza.Attributes;
using muza.Helpers;
using muza.Models;
using YoutubeExtractor;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;
using Ionic.Zip;

namespace muza.Controllers
{

    public class YoutubeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        // [MultipleButton(Name = "action", Argument = "Chuj")]
        public ActionResult Chuj(YoutubeModel model)
        {
            return View();
        }

        [HttpPost]
        public ActionResult PlayEmbedded(List<string> urls)
        {
            var model = YoutubeHelper.GetEmbeddedYoutubeResults(urls);         
            return PartialView(model);
        }

        [HttpPost]
        public ActionResult PlayAudio(YoutubeModel model)
        {
            return View(model);
        }

        [HttpGet]
        public ActionResult GetRawAudio(string url)
        {
            if (string.IsNullOrWhiteSpace(url))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var audioDownloader = new AudioDownloaderEntryClass(url);
            var title = string.Empty;
            var bytes = audioDownloader.GetMusicAsBytes(out title);
            var stream = new MemoryStream(bytes);
            return File(stream, "audio/wav");
        }

        [HttpPost]
        public ActionResult GetRawAudios(string urls)
        {
            if (string.IsNullOrWhiteSpace(urls))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var urlsparsed = new JavaScriptSerializer().Deserialize<List<string>>(urls);
            ZipFile zipFile = new ZipFile();

            foreach (var url in urlsparsed)
            {
                if (string.IsNullOrWhiteSpace(url))
                {
                    continue;
                }
                var trueUrl = url;
                if (trueUrl.Contains(@"&"))
                {
                    var split = url.Split('&')[0];
                    trueUrl = split;
                }

                var audioDownloader = new AudioDownloaderEntryClass(trueUrl);

                var title = string.Empty;
                byte[] fileBytes = null;
                string error = "";

                try
                {
                     fileBytes = audioDownloader.GetMusicAsBytes(out title);
                }
                catch (Exception e)
                {
                    error = $"nie dało się sciągnąć {url.Replace('/', ' ')}, sciagnij go recznie";
                   // throw new HttpException(, e);
                }

                if (fileBytes == null && !string.IsNullOrWhiteSpace(error))
                {
                    zipFile.AddEntry(error, new byte[] { });
                }
                else
                {
                    var fileName = title.Replace('/', ' ') + ".wav";
                    zipFile.AddEntry(fileName, fileBytes);
                }      
            }

            var stream = new MemoryStream();
            zipFile.Save(stream);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/zip", "jutub.zip");
        }

        public ActionResult Menu()
        {
            return PartialView();
        }
    }
}